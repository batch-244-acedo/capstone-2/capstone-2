const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderController");
const auth = require ("../auth");



// < ---------------------------- ADD TO CART --------------------------------->

router.post("/addToCart", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if (userData.isAdmin == false) {
		let data = {
			userId :  userData.id,
			productId : req.body.productId,
			quantity: req.body.quantity
		}
	console.log(userData)
	cartController.addToCart(data).then((resultFromController) => res.send(resultFromController));
	} else {
		res.send(false)
	}
});



// REMOVE PRODUCT FROM CART
router.patch("/:productId/remove", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	if (userData.isAdmin == false) {

		let data = {
			userId: userData.id,
			productId: req.params.productId
		}
	cartController.removeProduct(data).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false)
	}
});



// TOTAL PRICE FOR ALL ITEMS
router.get("/overAllTotal", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	let data = {
		userId :  userData.id,
	}
	cartController.overAllTotal(data).then((resultFromController) => res.send(resultFromController));
});


module.exports = router